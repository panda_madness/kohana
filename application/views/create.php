<!DOCTYPE html>
<html>
	<head>
		<title>Contacts</title>
	</head>
	<body>
		<?php if($is_post == FALSE) { ?>
			<h1><?= $message ?></h1>

			<form action="create" method="POST">
				<label for="first_name">First name</label>
				<input type="text" name="first_name"></input>
				<br />
				<label for="last_name">Last name</label>
				<input type="text" name="last_name"></input>
				<br />
				<label for="email">Email</label>
				<input type="text" name="email"></input>
				<br />
				<label for="phone_number">Phone number</label>
				<input type="text" name="phone_number"></input>
				<br />
				<label for="citizenship">Citizenship</label>
				<select name="citizenship">
					<?php foreach($countries as $country) { ?>
					<option value="<?= $country->id ?>"><?= $country->name ?></option>
					<?php } ?>
				</select>
				<br />
				<input type="submit" value="Create"></input>
			</form>
		<?php } ?>

		<?php if($is_post == TRUE) { ?>
			<h1><?= $message ?></h1>	
		<?php } ?>

		<h2><a href="/">Back</a></h2>
	</body>
</html>