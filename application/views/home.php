<!DOCTYPE html>
<html>
	<head>
		<title>Contacts</title>
	</head>
	<body>
		<h1>Contacts page</h1>
		<a href="welcome/create">Create new contact</a>
		<form action="welcome/handle" method="POST">
			<table>
				<tr>
					<td>Edit</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Email</td>
					<td>Phone Number</td>
					<td>Citizenship</td>
				</tr>
				<?php
					foreach ($data as $contact)
					{
						echo '<tr>';
						echo '<td><input type="radio" name="contact_id" value="'.$contact->id.'" /></td>';
						echo '<td>'.$contact->first_name.'</td>';
						echo '<td>'.$contact->last_name.'</td>';
						echo '<td>'.$contact->email.'</td>';
						echo '<td>'.$contact->phone_number.'</td>';
						echo '<td>'.$contact->citizenship.'</td>';
						echo '</tr>';
					}
				?>
			</table>
			<input type="submit" value="Edit" name="edit" />
			<input type="submit" value="Delete" name="delete" />
		</form>
	</body>
</html>