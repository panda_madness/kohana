<!DOCTYPE html>
<html>
	<head>
		<title>Contacts</title>
	</head>
	<body>
		<form action="update" method="POST">
			<input type="hidden" name="id" value="<?= $contact->id ?>"></input>
			<label for="first_name">First name</label>
			<input type="text" name="first_name" value="<?= $contact->first_name ?>"></input>
			<br />
			<label for="last_name">Last name</label>
			<input type="text" name="last_name" value="<?= $contact->last_name ?>"></input>
			<br />
			<label for="email">Email</label>
			<input type="text" name="email" value="<?= $contact->email ?>"></input>
			<br />
			<label for="phone_number">Phone number</label>
			<input type="text" name="phone_number" value="<?= $contact->phone_number ?>"></input>
			<br />
			<label for="citizenship">Citizenship</label>
			<select name="citizenship">
				<?php foreach($countries as $country) { ?>
				<option <?php if($contact->citizenship == $country->id) echo 'selected="selected"'; ?> value="<?= $country->id ?>"><?= $country->name ?></option>
				<?php } ?>
			</select>
			<br />
			<input type="submit" value="Submit"></input>
		</form>
			
		<h2><a href="/">Back</a></h2>
	</body>
</html>