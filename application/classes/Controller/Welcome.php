<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {

	private $model;
	private $cmodel;

	public function before()
	{
		$this->model = new Model_ContactsModel();
		$this->cmodel = new Model_CountriesModel();
	}

	public function action_index()
	{
		$contacts = $this->model->list_contacts();
		$view = View::factory('home')->bind('data', $contacts);
		$this->response->body($view);
	}

	public function action_create()
	{	
		if($this->request->post('first_name'))
		{
			$data['first_name']   = $this->request->post('first_name');
			$data['last_name']    = $this->request->post('last_name');
			$data['email']        = $this->request->post('email');
			$data['phone_number'] = $this->request->post('phone_number');
			$data['citizenship']  = $this->request->post('citizenship');

			if($this->model->create_contact($data))
			{
				$message = "Contact created";
			}
			else
			{
				$message = "Error";
			}
			
			$is_post = TRUE;
			$countries = null;
		}
		else
		{
			$is_post = FALSE;
			$countries = $this->cmodel->get_countries();
			$message = "Input data into fields";
		}

		$view = View::factory('create')
			->bind('is_post', $is_post)
			->bind('countries', $countries)
			->bind('message', $message);

		$this->response->body($view);
	}

	public function action_handle()
	{
		if($this->request->post('delete') == TRUE)
		{
			$id = $this->request->post('contact_id');
			if(isset($id))
			{
				$name = $this->model->delete_contact($id);
				if($name)
				{
					$message = "Contact deleted";
				}
			}
			else
			{
				$message = "Please select a contact";
			}

			$view = View::factory('delete')->bind('message', $message)->bind('name', $name);
		}

		if($this->request->post('edit') == TRUE)
		{
			$id = $this->request->post('contact_id');
			if(isset($id)) 
			{
				$contact = $this->model->get_contact($id);
				$countries = $this->cmodel->get_countries();

				$view = View::factory('update')
				->bind('countries', $countries)
				->bind('contact', $contact)
				->bind('message', $message);
			}
			else
			{
				$message = "Please select a contact";
				$view = View::factory('update_message')->bind('message', $message);
			}
		}
		
		$this->response->body($view);
	}

	public function action_update()
	{
		if($this->request->post('id'))
		{
			$data['id']           = $this->request->post('id');
			$data['first_name']   = $this->request->post('first_name');
			$data['last_name']    = $this->request->post('last_name');
			$data['email']        = $this->request->post('email');
			$data['phone_number'] = $this->request->post('phone_number');
			$data['citizenship']  = $this->request->post('citizenship');

			if($this->model->edit_contact($data))
			{
				$message = "Contact updated";
			}
			else
			{
				$message = "Error";
			}

			$view = View::factory('update_message')->bind('message', $message);
			$this->response->body($view);
		}
	}

} // End Welcome