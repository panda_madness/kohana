<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test extends Controller {

	public function action_index()
	{
		$this->response->body('hello, im test!');
	}

	public function action_create()
	{
		$this->response->body('hello, test create!');	
	}

	public function action_update()
	{
		$this->response->body('hello, test update!');
	}

	public function action_delete()
	{
		$this->response->body('hello, test delete!');
	}

} // End Welcome
