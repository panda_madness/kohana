<?php defined('SYSPATH') or die('No direct script access.');

class Model_CountriesModel extends Model
{
	public function get_countries()
	{
		$countries = ORM::factory('Country')->find_all()->as_array();
		return $countries;
	}
}