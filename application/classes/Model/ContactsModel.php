<?php defined('SYSPATH') or die('No direct script access.');

class Model_ContactsModel extends Model
{
	public function list_contacts()
	{
		$contacts = ORM::factory('Contact')->find_all()->as_array();

		foreach ($contacts as $contact)
		{
			$country = ORM::factory('Country', $contact->citizenship);
			$contact->citizenship = $country->name;
		}

		return $contacts;
	}

	public function create_contact($data)
	{
		$user = ORM::factory('Contact');

		$user->first_name   = $data['first_name'];
		$user->last_name    = $data['last_name']; 
		$user->email        = $data['email'];  
		$user->phone_number = $data['phone_number'];
		$user->citizenship  = $data['citizenship'];

		if($user->save())
			return TRUE;
		else
			return FALSE;
	}

	public function delete_contact($id)
	{
		$contact = ORM::factory('Contact', $id);

		$name = $contact->first_name.' '.$contact->last_name;

		if($contact->delete())
		{
			return $name;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_contact($id)
	{
		$contact = ORM::factory('Contact', $id);
		return $contact;
	}

	public function edit_contact($data)
	{
		$user = ORM::factory('Contact', $data['id']);

		$user->first_name   = $data['first_name'];
		$user->last_name    = $data['last_name']; 
		$user->email        = $data['email'];  
		$user->phone_number = $data['phone_number'];
		$user->citizenship  = $data['citizenship'];

		if($user->save())
			return TRUE;
		else
			return FALSE;
	}
}